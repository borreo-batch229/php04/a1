<?php require_once "./code.php"?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
</head>
<body>
	<h1>Access Modifiers</h1>
	<h2>Building Object</h2>
	<p><?php //echo $building->name; ?></p>

	<h2>Condominium Object</h2>
	<p><?php //echo $condominium->name; ?></p>

	<h1>Encapsulation</h1>
	<p>The name of the condominium is <?= $condominium->getName(); ?></p>

	

	<?php $condominium->setName("Enzo Tower"); ?>
	<p>The name of the condominium is <?= $condominium->getName() ?></p>
	<?php $condominium->setName("Enzo Condo"); ?>
	<p>The name of the condominium is <?= $condominium->getName() ?></p>
	

	<p><?php var_dump($building) ?></p>
	<p><?php var_dump($condominium) ?></p>

	<h1>ACTIVITY</h1>

	<h1>Building</h1>
		<p>The name of the building is <?= $building->getName(); ?>.</p>
		<p>The Caswynn Building has <?= $building->getFloors(); ?> floors.</p>
		<p>The Caswynn Building is located at <?= $building->getAddress(); ?>.</p>
					
		<?php $building->setName("Caswynn Complex"); ?>
		<p>The name of the building has been changed to <?= $building->getName() ?>.</p>

	<h1>Condominium</h1>
		<p>The name of the condominium is <?= $condominium->getName(); ?>.</p>
		<p>The Enzo Condo has <?= $condominium->getFloors(); ?> floors.</p>
		<p>The Enzo Condo is located at <?= $condominium->getAddress(); ?>.</p>
					
		<?php $condominium->setName("Enzo Tower"); ?>
		<p>The name of the building has been changed to <?= $condominium->getName() ?>.</p>


</body>
</html>