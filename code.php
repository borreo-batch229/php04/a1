<?php

class Building{
	// Access Modifiers - keywords that can be used to control the visibility of properties and methods in a class.
	// public: fully open, properties and emthods can be accessed from everywhere.
	// private: method or properties can only be accessed within the class and disables inheritance
	// protected: property or methods is only accessible within the class and on its child class

	protected $name;
	protected $floors;
	protected $address;

	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

		public function getName(){
		return $this->name;
	}

// setter - mutators used to modify the default value of a property in an instantiated object
	public function setName($name){
		//$this->name = $name;

		//setter functions can also be modified to add data validations.
		if (gettype($name) === "string"){
			$this->name = $name;
		}
	}

	// Getter/Setter Floor
		public function getFloors(){
		return $this->floors;
	}

		public function setFloors($floors){
		//$this->name = $name;
		if (gettype($floors) === "number"){
			$this->floors = $floors;
		}
	}

	// Getter/Setter Address
	public function getAddress(){
		return $this->address;
	}

			public function setAddress($address){
		//$this->name = $name;
		if (gettype($address) === "string"){
			$this->address = $address;
		}
	}
}

class Condominium extends Building{
// encapsulation indicates that data must not be directl accessibl to users but can be accessed through public functions (setter and getter function)

// getters and setters are used to retrieve and modify values of each property of the object.
// each property of an object should have a set of getter and setter function.
// getter - accessors used to retrieve value of an instantiated object
	public function getName(){
		return $this->name;
	}

// setter - mutators used to modify the default value of a property in an instantiated object
	public function setName($name){
		//$this->name = $name;

		//setter functions can also be modified to add data validations.
		if (gettype($name) === "string"){
			$this->name = $name;
		}
	}

	// Getter/Setter Floor
		public function getFloors(){
		return $this->floors;
	}

		public function setFloors($floors){
		//$this->name = $name;
		if (gettype($floors) === "number"){
			$this->floors = $floors;
		}
	}

	// Getter/Setter Address
	public function getAddress(){
		return $this->address;
	}

			public function setAddress($address){
		//$this->name = $name;
		if (gettype($address) === "string"){
			$this->address = $address;
		}
	}

}

$building = new Building("Caswynn Building", 8, "Timog Avenue, Quezon City, Philippines");
$condominium = new Condominium("Enzo Condo", 5, "Buendia Avenue, Makati City, Philippines");
